\d{1,2}(\/|\.)\d{1,2}(\/|\.)\d{2,4}

09.09.2019
01.11.22
11/11/1111
22/22/22
___________________________________________
^[\w\.-_]+@[\w\.-]+\.\w+$

sobaka@gmail.com
some.com
tuzik@bez
@urk.net
___________________________________________
<\/?[\w "=;\(,\)]+>

</p>
<h1 test="px">
<p
p>
p

___________________________________________
http[s]?:\/\/[\w-\.]+[\/]?  

https://tyest.com/
http://ttt.com
htp://aaa.com

___________________________________________
\d{1,3}\.\d{1,2}

22.22
333.11
1.22
333.1
11.0